/* infos
mysql -h zabbix.c7ydiyv3tdys.us-west-2.rds.amazonaws.com -P 3306 -u interview -p
2MLGz6q5py4mFNII
*/

/* - Query que retorna à quantidade de funcionários separados por sexo.*/
select gender as GENDER, count(*) as "COUNT" from employees group by gender;
 
/* test queries */
select count(gender) as GENDER_M from employees where gender = 'M';
select count(gender) as GENDER_F from employees where gender = 'F';

/* - Query que retorna à quantidade de funcionários distintos por sexo, ano e ano de nascimento.*/
select  count(DISTINCT gender, birth_date, hire_date) as DIST_COUNT from employees;

/* - Query que retorna a média, min e max de salário por sexo.*/
select employees.gender as GENDER, AVG(salary) as MEDIA, MAX(salary) as MAX, MIN(salary) as MIN from salaries JOIN employees on salaries.emp_no = employees.emp_no group by gender;

/* test queries */
select GENDER, AVG(salary) as MEDIA, MAX(salary) as MAX, MIN(salary) as MIN from salaries JOIN employees on salaries.emp_no = employees.emp_no and employees.gender = "M";
select GENDER, AVG(salary) as MEDIA, MAX(salary) as MAX, MIN(salary) as MIN from salaries JOIN employees on salaries.emp_no = employees.emp_no and employees.gender = "F";

