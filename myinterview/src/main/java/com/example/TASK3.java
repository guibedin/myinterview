package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {


    public static void main(String[] args) {

        // List of strings that will be generated
        List<String> stringList = new ArrayList<String>();
        Random r = new Random();

        // Chars that can be used to build a random string
        String letters = new String("abcdefghijklmnopqrstuvwxyz");

        // Maximum number of random strings
        int numberOfStrings = r.nextInt(2000);

        for(int i = 0; i <= numberOfStrings; i++) {
            StringBuilder string = new StringBuilder();

            // GGenerates a random string based on the letters, with maxium length of stringSize
            int stringSize = r.nextInt(15);
            for(int j = 0; j <= stringSize; j++) {
                string.append(letters.charAt(r.nextInt(letters.length())));
            }
            stringList.add(string.toString());

            // Adds the same string a second time
            if(stringList.size() % 3 == 0) {
                stringList.add(string.toString());
            }
        }

        int distinct = 0;
        List<String> distinctStringList = new ArrayList<String>();
        for(String s : stringList) {

            List<String> newStringList = new ArrayList<String>(stringList);
            newStringList.remove(s);

            if(!newStringList.contains(s)) {
                distinct++;
                distinctStringList.add(s);
            }
        }

        System.out.println(String.format("Original stringList size: %d", stringList.size()));
        System.out.println(String.format("stringList: %s", stringList.toString()));

        System.out.println(String.format("Number of distinct items: %d", distinct));
        System.out.println(String.format("distinctStringList: %s", distinctStringList.toString()));
    }
}
