package com.example;


/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {

    public static void main(String[] args) {

        System.out.println(isPalindrome("test"));
    }

    public static Boolean isPalindrome(String word) {

        StringBuilder reverse = new StringBuilder();

        // Creates the reverse string
        for(int i = word.length() -1; i >= 0; i--) {
            reverse.append(word.charAt(i));
        }

        // Returns true if the reversed and the word are equal, false otherwise
        return reverse.toString().equals(word);
    }
}
