package com.example;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */

// SCREEN RECORDING CRASHED AND I DIDN'T NOTICE UNTIL ALMOST SOLVING TASK2!
public class TASK2 {

    private TASK2 previous;
    private TASK2 next;
    private int index;
    private int currentSize;

    private int value;

    public static void main(String[] args) {

        // Creating list and adding new elements
        TASK2 list = new TASK2(5);

        list.addElement(10);
        list.addElement(20);
        list.addElement(30);
        list.addElement(40);
        list.addElement(50);
        list.addElement(60);


        // print list
        list.printList();
        // remove middle element
        list.removeMiddleElement();
        // print again
        list.printList();

        // remove first
        list.removeElementByIndex(0);
        // print again
        list.printList();

        // remove last
        list.removeElementByIndex(list.currentSize - 1);
        // print again
        list.printList();
    }

    // Constructor to create the list, initial value must be passed as argument
    public TASK2(int value) {
        this.previous = null;
        this.next = null;
        this.value = value;
        this.currentSize = 1;
        this.index = 0;
    }

    public TASK2(TASK2 previous, int value) {
        this.previous = previous;
        this.next = null;
        this.value = value;
        this.currentSize = previous.currentSize;
        this.index = previous.index + 1;
    }

    public void addElement(int value) {

        TASK2 currentElement = this;

        // Finds the last element of the list, increasing the current size stored by each element
        while(currentElement.getNext() != null) {
            currentElement.currentSize++;
            currentElement = currentElement.getNext();
        }

        // Increases the current size stored by the second to last element
        currentElement.currentSize++;
        currentElement.next = new TASK2(currentElement, value);
    }

    public TASK2 getNext() {
        return this.next;
    }

    public TASK2 getPrevious() {
        return this.previous;
    }

    // Removes middle element of the list
    public void removeMiddleElement() {
        // How to define middle if list has even number of elements?
        // Current solution will always remove the element closer to the end of the list
        int middleIndex = (this.currentSize / 2);
        this.removeElementByIndex(middleIndex);
    }

    // Not sure if the "middle" request is actually the middle element, or just any index
    // Logic here is very similar to "removeMiddle".
    // Actually, I will copy removeMiddle code here, and make removeMiddleElement call this one,
    // passing middle as parameter
    public void removeElementByIndex(int index) {

        TASK2 currentElement = this;

        if(index >= this.currentSize || index < 0)
            return;

        if(index == 0) {
            // Prints the middle element for the linked list
            System.out.println(String.format("Element to be removed: [Value = %d, Index = %d]\n",
                    currentElement.value, currentElement.index));

            TASK2 next = currentElement.getNext();
            next.previous = null;
            this.next = next.getNext();
            this.value = next.value;
            this.index = 1;
        } else {
            while (currentElement.index != index) {
                currentElement.currentSize--;
                currentElement = currentElement.getNext();
            }

            // Create the link from previous to next, "removing" the currentElement
            TASK2 previousElement = currentElement.previous;
            TASK2 nextElement = currentElement.next;
            if(previousElement != null)
                previousElement.next = nextElement;
            if(nextElement != null)
                nextElement.previous = previousElement;

            // Prints the middle element for the linked list
            System.out.println(String.format("Element to be removed: [Value = %d, Index = %d]\n",
                    currentElement.value, currentElement.index));
        }

        // Continues through the list, decreasing current size and index
        while(currentElement != null) {
            currentElement.currentSize--;
            currentElement.index--;
            currentElement = currentElement.getNext();
        }

    }

    // Prints the list
    private void printList() {
        TASK2 currentElement = this;

        StringBuilder list = new StringBuilder();

        list.append("==============================\n");
        list.append("List:\n");
        while(currentElement != null) {
            list.append(String.format("[Value = %d, Index = %d, List Size = %d]\n",
                    currentElement.value, currentElement.index, currentElement.currentSize));
            currentElement = currentElement.getNext();
        }
        list.append("==============================\n");
        System.out.println(list);
    }
}
