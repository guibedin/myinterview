package com.example;


import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Create an implementation of a Rest API client.
 * Prints out how many records exists for each gender and save this file to s3 bucket
 * API endpoint=> https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda 
 * AWS s3 bucket => interview-digiage
 *
 */

// TERMINOU SEM ERROS, ACREDITO QUE O UPLOAD FOI FEITO CORRETAMENTE PARA O S3
public class TASK4 {

    public static void main(String[] args) {

        try {
            // Makes the request for the API endpoint
            URL url = new URL("https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            InputStream inputStream = connection.getInputStream();
            StringBuilder output = new StringBuilder();

            // Reads the body of the response, creating a string
            int data = inputStream.read();
            while(data != -1) {
                output.append((char)data);
                data = inputStream.read();
            }

            // Converts string into array of object Employee, making it easier to count the records for each gender.
            Gson gson = new Gson();
            Employee[] employeeList = gson.fromJson(output.toString(), Employee[].class);

            int gender_M = 0;
            int gender_F = 0;

            for(Employee e : employeeList) {
                if(e.gender.equals("M"))
                    gender_M++;
                else
                    gender_F++;
            }

            // Writes the count to a file
            FileWriter fileWriter = new FileWriter("genders.txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);

            printWriter.println(String.format("Total records for gender M = %d\nTotal records for gender F = %d",
                    gender_M, gender_F));
            printWriter.close();

            // Save file in S3 bucket
            saveToS3("genders.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void saveToS3(String fileName) {
        Regions clientRegion = Regions.DEFAULT_REGION;
        String bucketName = "interview-digiage";
        //String stringObjKeyName = "string obj key name";
        String fileObjKeyName = "File obj key name";


        // Code from AWS example - https://docs.aws.amazon.com/pt_br/AmazonS3/latest/dev/UploadObjSingleOpJava.html
        try {
            //This code expects that you have AWS credentials set up per:
            // https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(clientRegion)
                    .build();

            // Upload a text string as a new object.
            //s3Client.putObject(bucketName, stringObjKeyName, "Uploaded String Object");

            // Upload a file as a new object with ContentType and title specified.
            PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, new File(fileName));
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("plain/text");
            metadata.addUserMetadata("title", "TASK 4 - DigiAge - Guilherme Bedin");
            request.setMetadata(metadata);
            s3Client.putObject(request);
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }

    }

    private class Employee {

        private String gender;
        private int emp_number;
        private String last_name;
        private String first_name;

        public Employee(String gender, int emp_number, String last_name, String first_name) {
            this.gender = gender;
            this.emp_number = emp_number;
            this.last_name = last_name;
            this.first_name = first_name;
        }
    }
}